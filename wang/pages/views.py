#AUTHORS:

#Gabriel Lizano Alvarado
#Edwin Lobo Sánchez 
#Ruben Pereira Ugalde
#Luis Guillermo Ramírez Díaz

from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.http import JsonResponse
from .models import Page
from .forms import RequestForm
from datetime import datetime
import requests
import json

# Create your views here.

def index(request,pagename):
    # This is for return the page.html with the form.
    submitted = False
    approved = True
    form = RequestForm()
    return render(request,'pages/page.html', { 'form': form, 'page_list': Page.objects.all(), 
                    'submitted': submitted, 'approved': approved } )

def page_deductions(request):
    # This is for return the page2.html with the table with the
    # deductions.
    print("Inside page_deductions request.")
    dict = get_deductions()
    dict = list ( map(lambda x: dating(x), dict) )
    context = {
        "deductions" : dict
    }
    return render(request, "pages/page2.html", context)
    
def dating(data):
    new_date = data['created'][:19]
    new_date = new_date.replace('T',' ')
    data['created'] = new_date
    return data

# Method to verify if the expression have all the requirements to be
# evaluated.

def verify_in_api(data_post):
    # This use 201 if is created...
    r = requests.post('http://localhost:8080/api/deduction/', data = data_post)
    if r.status_code == 201:
        ded_json = r.json()
        return ded_json
    return False

# Methods that are used when I use AJAX.

@csrf_exempt
def request(request):
    if request.method == 'POST':
        deduc = verify_in_api(request.POST)
        if not deduc:
            return None
        return get_deduction_and_details(deduc.get('id'))

@csrf_exempt
def request_all_tree(request):
    a = request.POST.get('formula_id')
    return get_deduction_and_details(a)

# Methods for get the Deductions with the Web-API. 

def get_deductions():
    r = requests.get('http://localhost:8080/api/deduction/all')
    if r.status_code == 200:
        return r.json()
    return None

def get_deduction(id):
    r = requests.get(f'http://localhost:8080/api/deduction/{id}')
    if r.status_code == 200:
        return r.json()
    else:
        return False

# Methods for get all the details of the deduction.
# This are the new expressions that generates the proof testing.
        
def get_deduc_details(deduction_id):
    r = requests.get(f'http://localhost:8080/api/detail/spec/{deduction_id}')
    if r.status_code == 200:
        return r.json()
    else:
        return False

def get_deduction_and_details(id):
    r = requests.get(f'http://localhost:8080/api/deduction/{id}')
    if r.status_code == 200:
        deduc = r.json()
        details = get_deduc_details(deduc.get('id'))
        details.insert(0, deduc)
        return JsonResponse(json.dumps(details), safe = False)
    return None
