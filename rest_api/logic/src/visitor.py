__author__ = 'jszheng'
__coauthor__ = 'loriacarlos@gmail.com'

import os
from WangParser import WangParser
from WangVisitor import WangVisitor
from deduction import *
from formula import *

"""
    Tests Wang Visitor
    See grammar/Wang.g4
    based on jszheng git https://github.com/jszheng/py3antlr4book
    Example calc
"""

class WangPrintVisitor(WangVisitor):
    def __init__(self):
        pass
    
    def visitAssertion(self, ctx):
        children = ctx.deduction()
        deductions = [self.visit(ch) for ch in children]
        return deductions
        
    def visitDeducExpr(self, ctx):
        left = self.visit(ctx.sequence(0))
        right = self.visit(ctx.sequence(1))
        if left == None: left = []
        if right == None: right =[]
        return Deduction(left, right)


    def visitSeqExpr(self, ctx):
        #print(f'Visiting SeqExpr (,) with {len(ctx.expr())} children')
        children = ctx.expr() 
        return [ self.visit(ch) for ch in children ]
    
    
    def visitId(self, ctx):
        #print(f'Visiting the Atom expression')
        name = ctx.ID().getText()
        return Atom(name)

    def visitAndExpr(self, ctx):
        #print('Visiting AndExpr (&)')
        left = self.visit( ctx.expr(0) )
        right = self.visit( ctx.expr(1) )
        #print("***** After visiting AndExpr *******")
        return And(left, right)

    def visitOrExpr(self, ctx):
        #print('Visiting OrExpr (|)')
        left = self.visit( ctx.expr(0) )
        right = self.visit( ctx.expr(1) )
        #print("***** After visiting OrExpr *******")
        return Or(left, right)
       
    def visitThenExpr(self, ctx):
        left = self.visit( ctx.expr(0) )
        right = self.visit( ctx.expr(1) )
        #print("***** After visiting ThenExpr *******")
        return Then(left, right)
    
    def visitBiExpr(self, ctx):
        #print ('Visiting BiExpr (<->)')
        left = self.visit( ctx.expr(0) )
        right = self.visit( ctx.expr(1) )
        #print("***** After visiting BiExpr *******")
        return Bidirection(left, right)

    def visitParens(self, ctx):
        # print('Visiting ParenExpr (...)')
        return self.visit(ctx.expr())

    def visitNotExpr(self, ctx):
        #print('Visiting NotExpr (~) ')
        exp = self.visit(ctx.expr())
        return Not(exp)

