#AUTHORS:

#Gabriel Lizano Alvarado
#Edwin Lobo Sánchez 
#Ruben Pereira Ugalde
#Luis Guillermo Ramírez Díaz

from django.db import models

# Create your models here.

TYPE_CHOICES = (
    ('1', 'WANG'),
    ('2', 'LORÍA'),
)

class Page(models.Model):
    title = models.CharField(max_length=60)
    permalink = models.CharField(max_length=12, unique=True)
    update_date = models.DateTimeField('Last Updated')
    bodytext = models.TextField('Page Content', blank=True)

    def __str__(self):
        return self.title

# Models to connect to the Web API.

class deduction(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    formula = models.CharField(max_length = 200)
    resolve_type = models.CharField(max_length = 20, choices = TYPE_CHOICES)
    
    def __str__(self):
        return f"{self.formula} {self.resolve_type}"
