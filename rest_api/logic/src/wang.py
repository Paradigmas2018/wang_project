"""
    Tests Wang Parser
    See grammar/Wang.g4
    based on jszheng git https://github.com/jszheng/py3antlr4book Example calc
    Extended by loriacarlos@gmail.com
    
"""
__author__ = 'jszheng'
__coauthor__ = 'loriacarlos@gmail.com'

import sys
from antlr4 import *
from antlr4.error.ErrorListener import ErrorListener, ConsoleErrorListener
from antlr4.InputStream import InputStream
from WangLexer import WangLexer
from WangParser import WangParser
from visitor import WangPrintVisitor
from formula import *

class MyErrorListener(ErrorListener):
    """
        Based on https://stackoverflow.com/questions/18132078/handling-errors-in-antlr4
    """
    def syntaxError(self,  recognizer, 
                            offendingSymbol,
                            line, 
                            charPositionInLine,
                            msg, 
                            e):
        error_msg = f"{self.__class__.__name__}: line {line}: {charPositionInLine} {msg}"
        raise SyntaxError(error_msg)



class ParsingExpr:
    """
        This class is for evaluate if the expression is correctly with the grammar.
    """
    def __init__(self):
        pass
    
    def my_visit(self, formula):
        print("*** Testing Wang Parser (EIF400 II-2018) ***")
        print(f'*** Processing from web interface ***\n>', end='')
        to_parse_line = formula
        #Use demo line if none was typed
        if len(to_parse_line) <= 1: # Just enter was hit
            print("Empty line")
            return None
        
        input_stream = InputStream(to_parse_line)

        #Setup Lexer 
        lexer = WangLexer(input_stream)
        token_stream = CommonTokenStream(lexer)

        #Setup Parser (and own ErrorListener)
        parser = WangParser(token_stream)
        parser.removeErrorListeners()
        parser.addErrorListener(MyErrorListener())
        try:
            tree = parser.assertion()
        except SyntaxError as e:
            # Returns as a string the error message.
            return(e.msg)
            sys.exit(-1)

        #Setup the Visitor and visit Parse tree
        visitor = WangPrintVisitor()
        print("*** Starts visit of data ***")
        # This return the list of the deductions, if they apply all the 
        # grammar rules
        deduc_list = ( visitor.visit(tree) )
        # print(type(deduc_list))
        return deduc_list


if __name__ == '__main__':
    #Some cases for use.
    p = ParsingExpr()
    iter = p.my_visit("=> (p & q -> r) -> ( (p -> r) | (q -> r) )")
    print("**** After visiting all the trees ****")
    if type(iter) == list:
        for i in iter:
            print(i)

