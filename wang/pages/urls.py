#AUTHORS:

#Gabriel Lizano Alvarado
#Edwin Lobo Sánchez 
#Ruben Pereira Ugalde
#Luis Guillermo Ramírez Díaz

from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [

    path('ajax/request', views.request, name='request'),
    path('ajax/request_all_tree', views.request_all_tree, name='request_all_tree'),
    path('page_deductions', views.page_deductions, name='page_deductions'),
    url(r'([^/]*)', views.index, name='index'),

]