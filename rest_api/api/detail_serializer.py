#AUTHORS:

#Gabriel Lizano Alvarado
#Edwin Lobo Sánchez 
#Ruben Pereira Ugalde
#Luis Guillermo Ramírez Díaz

from rest_framework import serializers
from .models import DeductionDetail


class DeductionDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeductionDetail
        fields = ('id', 'new_expr','deduction','father_detail', 'rule_applied')
    
    def create(self, data):
        print(f"The data that you are entering is {data}")
        return super().create(data)
    
    def update(self, instance, data):
        instance.new_expr = data.get('new_expr', instance.new_expr)
        instance.deduction = data.get('deduction', instance.deduction)
        instance.father_detail = data.get('father_detail', instance.father_detail)
        instance.rule_applied = data.get('rule_applied', instance.rule_applied)
        instance.save()
        return instance