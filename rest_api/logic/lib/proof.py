
"""
Prototipo de modelo de objetos para Wang
loriacarlos@gmail.com
@since II-2018
"""

from deduction import *
from formula import *
from enum import Enum, auto
from abc import ABCMeta, abstractmethod
from formula import *
import utils
from itertools import chain
import requests
from functools import reduce, partial

class RuleType(Enum):
    AXIOM = auto()
    OR_LEFT = auto()
    OR_RIGHT = auto()
    AND_LEFT = auto()
    AND_RIGHT = auto()
    EQUIV = auto()
    NOT = auto()
    BIDIRECTION = auto()

class Just:
    def __init__(self, subject, rule):
        self.subject = subject
        self.rule = rule
        
class Rule(metaclass=ABCMeta):
    def __init__(self, ruletype):
        self.__ruletype = ruletype
    @abstractmethod
    def apply(self, deduction): pass
    @property
    def kind(self):
        return self.__ruletype

class Axiom(Rule):
    def __init__(self):
        super().__init__(RuleType.AXIOM)
    
    def apply(self, deduction):
        for (pi, p) in enumerate(deduction.left):
            if p in deduction.right:
                yield (self.kind, pi, deduction)
        
class AndLeft(Rule):
    def __init__(self):
        super().__init__(RuleType.AND_LEFT)
    def apply(self, deduction):
        yield from ( (self.kind, p, Deduction(list(utils.replace(deduction.left, p, [f.left, f.right])), deduction.right) ) for p,f in enumerate(deduction.left) if isinstance(f,And))

class OrLeft(Rule):
    def __init__(self):
        super().__init__(RuleType.OR_LEFT)
    def apply(self, deduction):
        yield from ( (self.kind, p, 
        Deduction(list(utils.replace(deduction.left, p, [f.left])), deduction.right), 
        Deduction(list(utils.replace(deduction.left, p, [f.right])), deduction.right) )
        for p,f in enumerate(deduction.left) 
        if isinstance(f,Or) )

class OrRight(Rule):
    def __init__(self):
        super().__init__(RuleType.OR_RIGHT)
    def apply(self, deduction):
        yield from ( (self.kind, p, Deduction(deduction.left, list(utils.replace(deduction.right, p, [f.left, f.right]))) ) 
        for p,f in enumerate(deduction.right) 
        if isinstance(f,Or))

class AndRight(Rule):
    def __init__(self):
        super().__init__(RuleType.AND_RIGHT)
    def apply(self, deduction):
        yield from ( (self.kind, p, 
        Deduction(deduction.left, list(utils.replace(deduction.right, p, [f.left])) ), 
        Deduction(deduction.left, list(utils.replace(deduction.right, p, [f.right])) ) )  
        for p,f in enumerate(deduction.right) 
        if isinstance(f, And) )

class Equiv(Rule):
    def __init__(self):
        super().__init__(RuleType.EQUIV)
    def apply(self, deduction):
        yield from ( (self.kind, p, Deduction(list(utils.replace(deduction.left, p, [Or(Not(f.left),f.right)])), deduction.right ) )
        for p, f in enumerate(deduction.left) 
        if isinstance(f,Then) )

        yield from (  (self.kind, p, Deduction(deduction.left, list(utils.replace(deduction.right, p, [Or(Not(f.left),f.right)]))) )
        for p, f in enumerate(deduction.right) 
        if isinstance(f,Then) )

class NotRule(Rule):
    def __init__(self):
        super().__init__(RuleType.NOT)
    def apply(self, deduction):
        yield from (  (self.kind, p, Deduction([x for x in deduction.left if x != f], deduction.right+[f.left]) )
        for p, f in enumerate(deduction.left) 
        if isinstance(f,Not) )

        yield from ( (self.kind, p, Deduction(deduction.left+[f.left], [x for x in deduction.right if x != f]) )
        for p, f in enumerate(deduction.right) 
        if isinstance(f,Not) )
  
class BidirectionRule(Rule):
    def __init__(self):
        super().__init__(RuleType.BIDIRECTION)
    def apply(self, deduction):
        yield from ( (self.kind, p, Deduction(list(utils.replace(deduction.left, p, [ And(Then(f.left, f.right), Then(f.right, f.left)) ])), deduction.right) )
        for p, f in enumerate(deduction.left) 
        if isinstance(f, Bidirection) )

        yield from ( (self.kind, p, Deduction(deduction.left, list(utils.replace(deduction.right, p, [ And(Then(f.left, f.right), Then(f.right, f.left)) ])) ) )
        for p,f in enumerate(deduction.right) 
        if isinstance(f, Bidirection) )   

AND_LEFT_RULE= AndLeft()   
OR_LEFT_RULE= OrLeft()        
OR_RIGHT_RULE= OrRight()
AND_RIGHT_RULE= AndRight()  
NOT_RULE= NotRule()       
EQUIV_RULE= Equiv()    
BIDIRECTION_RULE = BidirectionRule()
AXIOM_RULE = Axiom()

def applyRules(deduction):
    yield from AXIOM_RULE.apply(deduction)
    yield from NOT_RULE.apply(deduction)
    yield from AND_LEFT_RULE.apply(deduction)
    yield from OR_RIGHT_RULE.apply(deduction)
    yield from OR_LEFT_RULE.apply(deduction)
    yield from AND_RIGHT_RULE.apply(deduction)
    yield from EQUIV_RULE.apply(deduction)
    yield from BIDIRECTION_RULE.apply(deduction)
    yield ("Can not apply any rule", -1, deduction)

def pipe(init, *iterables):
    return reduce(lambda a, n: n(a), iterables, init)  

def firstRule(func):
    return utils.first(func)

def findAxiom(id_root, tup):
    if tup[1]==-1:
        return False
    elif tup[0]==RuleType.AXIOM:
        return True
    elif tup[0]==RuleType.OR_LEFT or tup[0]==RuleType.AND_RIGHT:
        return prooving(tup[2], id_root, tup[4]) and prooving(tup[3],id_root,tup[5])
    return prooving(tup[2],id_root, tup[3])


def saveBD(id_root, id_father, tup):
    data_post = { "new_expr": str(tup[2]), "deduction": id_root, "father_detail": id_father, "rule_applied": tup[0] }
    r = requests.post('http://localhost:8080/api/detail/', data = data_post)
    #r = requests.post('http://deduction-rest-api.herokuapp.com/api/detail/', data = data_post)
    tup = (*tup, r.json().get('id'))
    if tup[0]==RuleType.OR_LEFT or tup[0]==RuleType.AND_RIGHT:
        data_post = { "new_expr": str(tup[3]), "deduction": id_root, "father_detail": id_father, "rule_applied": tup[0] }
        r = requests.post('http://localhost:8080/api/detail/', data = data_post)
        #r = requests.post('http://deduction-rest-api.herokuapp.com/api/detail/', data = data_post)
        tup = (*tup, r.json().get('id'))
    return tup

def prooving(deduction, id_root, id_father=None):
    partial_saveBD = partial(saveBD, id_root, id_father)
    partial_findAxiom = partial(findAxiom, id_root)
    p = pipe(
        deduction,
        applyRules,
        firstRule,
        partial_saveBD, 
        partial_findAxiom
    )
    return p
            

            
        
if __name__ == "__main__":
    #1. ~~p => p LISTO. 
    #2. p => ~~p. 
    # p & ~p => r.
    # (p -> q) & p => q & p. 
    # (p -> q) & ~q => ~p. 
    # => p | ~ p.
    # (p -> q) & (q -> p) => p <=> q SO-SO (MUY LARGO).
    # => p <=> p.
    # p <=> q => q <=> p.


    p = Atom("p")
    q = Atom("q")
    opq = Or(p,q)
    apq = And(p,q)
    tpq = Then(p,q)
    tqp = Then(q,p)
    bpq = Bidirection(p,q)
    
    ded = Deduction(  [  ] , [ Bidirection(p,p) ] ) 
    print(ded)
    print("EXITOOOO!!!") if prooving( ded )  else print("RAYOS, NO SE PUDO PROBAR")
