#AUTHORS:

#Gabriel Lizano Alvarado
#Edwin Lobo Sánchez 
#Ruben Pereira Ugalde
#Luis Guillermo Ramírez Díaz

from django import forms
from django.forms import ModelForm
from .models import deduction

class RequestForm(forms.ModelForm):
    # This is for the css or bootstrap.
    required_css_class = 'required'
    class Meta:
        model = deduction
        fields = [
            'formula', 'resolve_type'
        ]
