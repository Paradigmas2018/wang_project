      google.charts.load('current', {packages:["orgchart"]});

      function drawChart(datos){
          var data = new google.visualization.DataTable();          
          let info = JSON.parse(datos);
          let deduction = info[0];          
          let deductiondetails = info.slice(1);
          rowsArray = deductiondetails.map(k=> k.father_detail? addtoArray(k.id, k.new_expr, k.father_detail, k.rule_applied) : addtoArray(k.id, k.new_expr, 0, k.rule_applied) )
          rowsArray.unshift(addtoArray(0, deduction.formula))
          data.addColumn('string', 'id');
          data.addColumn('string', 'Name');
          data.addColumn('string', 'ToolTip');
          data.addRows(rowsArray);
		  let head = document.getElementById('chart_head');
		  head.innerHTML = "<h3>Expression's Tree </h3>"
          var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));
          chart.draw(data, {allowHtml:true, allowCollapse:true,axis: 'horizontal', keepInBounds: true, nodeClass:'maman'});       
      }

      function addtoArray(id,new_expr, father=null, rule=null){  
         let obj = new Object;
         let array=[];
         if(father===null){ 
             obj.v = id.toString();
             // obj.f= new_expr+'<div style="color:green; font-style:italic">Deduction ('+id+')  </div>';
             obj.f= new_expr;
             array.push(obj);
             array.push('');
             array.push('');
         }
         else{
             obj.v = id.toString();
             if(rule==="RuleType.AXIOM"){
                obj.f= '<div class="text-success">'+rule+'</div>';
             }
             else if(rule==="Can not apply any rule"){
                obj.f= '<div class="text-danger">'+rule+'</div>';
             }
             else{
                 obj.f= new_expr+'<div class="text-info"">'+rule+'</div>';
             }
             array.push(obj);
             array.push(father.toString());
             array.push('');    
         }
         return array;         
      }     

