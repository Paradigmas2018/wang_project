# Generated from .\grammar\Wang.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\17")
        buf.write("B\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\3\2\3\2")
        buf.write("\7\2\20\n\2\f\2\16\2\23\13\2\3\2\3\2\3\3\3\3\3\3\5\3\32")
        buf.write("\n\3\3\4\5\4\35\n\4\3\5\3\5\3\5\7\5\"\n\5\f\5\16\5%\13")
        buf.write("\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6/\n\6\3\6\3\6\3")
        buf.write("\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\7\6=\n\6\f\6\16")
        buf.write("\6@\13\6\3\6\2\3\n\7\2\4\6\b\n\2\2\2F\2\f\3\2\2\2\4\26")
        buf.write("\3\2\2\2\6\34\3\2\2\2\b\36\3\2\2\2\n.\3\2\2\2\f\21\5\4")
        buf.write("\3\2\r\16\7\6\2\2\16\20\5\4\3\2\17\r\3\2\2\2\20\23\3\2")
        buf.write("\2\2\21\17\3\2\2\2\21\22\3\2\2\2\22\24\3\2\2\2\23\21\3")
        buf.write("\2\2\2\24\25\7\2\2\3\25\3\3\2\2\2\26\31\5\6\4\2\27\30")
        buf.write("\7\7\2\2\30\32\5\6\4\2\31\27\3\2\2\2\31\32\3\2\2\2\32")
        buf.write("\5\3\2\2\2\33\35\5\b\5\2\34\33\3\2\2\2\34\35\3\2\2\2\35")
        buf.write("\7\3\2\2\2\36#\5\n\6\2\37 \7\5\2\2 \"\5\n\6\2!\37\3\2")
        buf.write("\2\2\"%\3\2\2\2#!\3\2\2\2#$\3\2\2\2$\t\3\2\2\2%#\3\2\2")
        buf.write("\2&\'\b\6\1\2\'(\7\b\2\2(/\5\n\6\t)/\7\r\2\2*+\7\3\2\2")
        buf.write("+,\5\n\6\2,-\7\4\2\2-/\3\2\2\2.&\3\2\2\2.)\3\2\2\2.*\3")
        buf.write("\2\2\2/>\3\2\2\2\60\61\f\b\2\2\61\62\7\t\2\2\62=\5\n\6")
        buf.write("\t\63\64\f\7\2\2\64\65\7\n\2\2\65=\5\n\6\b\66\67\f\6\2")
        buf.write("\2\678\7\13\2\28=\5\n\6\69:\f\4\2\2:;\7\f\2\2;=\5\n\6")
        buf.write("\4<\60\3\2\2\2<\63\3\2\2\2<\66\3\2\2\2<9\3\2\2\2=@\3\2")
        buf.write("\2\2><\3\2\2\2>?\3\2\2\2?\13\3\2\2\2@>\3\2\2\2\t\21\31")
        buf.write("\34#.<>")
        return buf.getvalue()


class WangParser ( Parser ):

    grammarFileName = "Wang.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'('", "')'", "','", "'.'", "'=>'", "'~'", 
                     "'&'", "'|'", "'->'", "'<=>'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "COMMA", "DOT", 
                      "LEADSTO", "NOT", "AND", "OR", "IMPLIES", "BIDIR", 
                      "ID", "WS", "ErrorChar" ]

    RULE_assertion = 0
    RULE_deduction = 1
    RULE_sequence = 2
    RULE_listexpr = 3
    RULE_expr = 4

    ruleNames =  [ "assertion", "deduction", "sequence", "listexpr", "expr" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    COMMA=3
    DOT=4
    LEADSTO=5
    NOT=6
    AND=7
    OR=8
    IMPLIES=9
    BIDIR=10
    ID=11
    WS=12
    ErrorChar=13

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class AssertionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def deduction(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WangParser.DeductionContext)
            else:
                return self.getTypedRuleContext(WangParser.DeductionContext,i)


        def EOF(self):
            return self.getToken(WangParser.EOF, 0)

        def getRuleIndex(self):
            return WangParser.RULE_assertion

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssertion" ):
                return visitor.visitAssertion(self)
            else:
                return visitor.visitChildren(self)




    def assertion(self):

        localctx = WangParser.AssertionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_assertion)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 10
            self.deduction()
            self.state = 15
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WangParser.DOT:
                self.state = 11
                self.match(WangParser.DOT)
                self.state = 12
                self.deduction()
                self.state = 17
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 18
            self.match(WangParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DeductionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WangParser.RULE_deduction

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class DeducExprContext(DeductionContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WangParser.DeductionContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def sequence(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WangParser.SequenceContext)
            else:
                return self.getTypedRuleContext(WangParser.SequenceContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDeducExpr" ):
                return visitor.visitDeducExpr(self)
            else:
                return visitor.visitChildren(self)



    def deduction(self):

        localctx = WangParser.DeductionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_deduction)
        self._la = 0 # Token type
        try:
            localctx = WangParser.DeducExprContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 20
            self.sequence()
            self.state = 23
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==WangParser.LEADSTO:
                self.state = 21
                self.match(WangParser.LEADSTO)
                self.state = 22
                self.sequence()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SequenceContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def listexpr(self):
            return self.getTypedRuleContext(WangParser.ListexprContext,0)


        def getRuleIndex(self):
            return WangParser.RULE_sequence

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSequence" ):
                return visitor.visitSequence(self)
            else:
                return visitor.visitChildren(self)




    def sequence(self):

        localctx = WangParser.SequenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_sequence)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 26
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << WangParser.T__0) | (1 << WangParser.NOT) | (1 << WangParser.ID))) != 0):
                self.state = 25
                self.listexpr()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ListexprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WangParser.RULE_listexpr

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class SeqExprContext(ListexprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WangParser.ListexprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WangParser.ExprContext)
            else:
                return self.getTypedRuleContext(WangParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSeqExpr" ):
                return visitor.visitSeqExpr(self)
            else:
                return visitor.visitChildren(self)



    def listexpr(self):

        localctx = WangParser.ListexprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_listexpr)
        self._la = 0 # Token type
        try:
            localctx = WangParser.SeqExprContext(self, localctx)
            self.enterOuterAlt(localctx, 1)
            self.state = 28
            self.expr(0)
            self.state = 33
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==WangParser.COMMA:
                self.state = 29
                self.match(WangParser.COMMA)
                self.state = 30
                self.expr(0)
                self.state = 35
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return WangParser.RULE_expr

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class AndExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WangParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WangParser.ExprContext)
            else:
                return self.getTypedRuleContext(WangParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAndExpr" ):
                return visitor.visitAndExpr(self)
            else:
                return visitor.visitChildren(self)


    class ThenExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WangParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WangParser.ExprContext)
            else:
                return self.getTypedRuleContext(WangParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitThenExpr" ):
                return visitor.visitThenExpr(self)
            else:
                return visitor.visitChildren(self)


    class BiExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WangParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WangParser.ExprContext)
            else:
                return self.getTypedRuleContext(WangParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBiExpr" ):
                return visitor.visitBiExpr(self)
            else:
                return visitor.visitChildren(self)


    class ParensContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WangParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(WangParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParens" ):
                return visitor.visitParens(self)
            else:
                return visitor.visitChildren(self)


    class NotExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WangParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(WangParser.ExprContext,0)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNotExpr" ):
                return visitor.visitNotExpr(self)
            else:
                return visitor.visitChildren(self)


    class IdContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WangParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(WangParser.ID, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitId" ):
                return visitor.visitId(self)
            else:
                return visitor.visitChildren(self)


    class OrExprContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a WangParser.ExprContext
            super().__init__(parser)
            self.op = None # Token
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(WangParser.ExprContext)
            else:
                return self.getTypedRuleContext(WangParser.ExprContext,i)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOrExpr" ):
                return visitor.visitOrExpr(self)
            else:
                return visitor.visitChildren(self)



    def expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = WangParser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 8
        self.enterRecursionRule(localctx, 8, self.RULE_expr, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 44
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [WangParser.NOT]:
                localctx = WangParser.NotExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 37
                self.match(WangParser.NOT)
                self.state = 38
                self.expr(7)
                pass
            elif token in [WangParser.ID]:
                localctx = WangParser.IdContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 39
                self.match(WangParser.ID)
                pass
            elif token in [WangParser.T__0]:
                localctx = WangParser.ParensContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 40
                self.match(WangParser.T__0)
                self.state = 41
                self.expr(0)
                self.state = 42
                self.match(WangParser.T__1)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 60
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,6,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 58
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
                    if la_ == 1:
                        localctx = WangParser.AndExprContext(self, WangParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 46
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 47
                        localctx.op = self.match(WangParser.AND)
                        self.state = 48
                        self.expr(7)
                        pass

                    elif la_ == 2:
                        localctx = WangParser.OrExprContext(self, WangParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 49
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 50
                        localctx.op = self.match(WangParser.OR)
                        self.state = 51
                        self.expr(6)
                        pass

                    elif la_ == 3:
                        localctx = WangParser.ThenExprContext(self, WangParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 52
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 53
                        localctx.op = self.match(WangParser.IMPLIES)
                        self.state = 54
                        self.expr(4)
                        pass

                    elif la_ == 4:
                        localctx = WangParser.BiExprContext(self, WangParser.ExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 55
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 56
                        localctx.op = self.match(WangParser.BIDIR)
                        self.state = 57
                        self.expr(2)
                        pass

             
                self.state = 62
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,6,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[4] = self.expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 5)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 4)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 2)
         




