/*
@author loriacarlos@gmail.com
@since II-2018
*/


grammar Wang;

assertion: deduction ('.' deduction)* EOF
;

deduction: sequence ('=>'  sequence)? #DeducExpr
;

sequence: listexpr?           
;

listexpr: expr (',' expr)*   # SeqExpr
;

expr:   '~' expr              # NotExpr
    |   expr op='&' expr      # AndExpr
    |   expr op='|' expr      # OrExpr
    |   <assoc=right>  
        expr op='->' expr     # ThenExpr
    |   ID                    # Id
    |   <assoc=right>
        expr op='<=>' expr    # BiExpr
    |   '(' expr ')'          # Parens
;

COMMA : ','
;
DOT : '.'
;
LEADSTO : '=>'
;
NOT : '~'
;
AND : '&' 
; 
OR :  '|' 
;
IMPLIES: '->'
;
BIDIR: '<=>'
;
ID  :   [a-z][a-z0-9_]* 
;      
WS  :   [\r\n\t ]+ -> skip
;
ErrorChar : .
;