@echo off

REM AUTHORS:

REM Gabriel Lizano Alvarado
REM Edwin Lobo Sánchez 
REM Ruben Pereira Ugalde
REM Luis Guillermo Ramírez Díaz



REM .bat for set the pythonpath with our models.

set SOURCE=.\logic\src
set LIB=.\logic\lib
set PARSER=.\logic\parser\grammar
set GRAMMAR=.\logic\grammar

SET PYTHONPATH=%SOURCE%;%LIB%;%PARSER%;%GRAMMAR%;%PYTHONPATH%
echo *** PYTHONPATH=%PYTHONPATH% ***

.\logic\bats\antlr4env.bat

