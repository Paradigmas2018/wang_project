#AUTHORS:

#Gabriel Lizano Alvarado
#Edwin Lobo Sánchez 
#Ruben Pereira Ugalde
#Luis Guillermo Ramírez Díaz

from rest_framework import serializers
from .models import deduction
from deduction import Deduction
from wang import ParsingExpr
from proof import *

class DeductionSerializer(serializers.ModelSerializer):
    class Meta:
        model = deduction
        # fields = ('created', 'formula', 'resolve_type')
        fields = ('id', 'formula', 'resolve_type')

    def create(self, data):
        """
        Create a new Deduction to the database.
        """
        #Here I call the method to verify if the expression
        #is correctly.
        print(f'--> The data that you are entering is {data}')
        list_dec = validate(**data)
        if type(list_dec) == list:
            print('Expression approved')
            ded = deduction.objects.create(**data)
            id = ded.id
            # Here I connect with proof.
            prooving(list_dec[0], id)
            # Return the complete deduction.
            return ded
        print('Expression not - approved')
        # This method throw an exception
        raise serializers.ValidationError('Expression not - approved')
    
    def update(self, instance, data):
        """
        Update and return an existing Deduction instance, given the validated data.
        """
        instance.created = data.get('created', instance.created)
        instance.formula = data.get('formula', instance.formula)
        instance.resolve_type = data.get('resolve_type', instance.resolve_type)
        instance.save()
        return instance
        

def validate(formula, resolve_type):
    m = ParsingExpr()
    ded = m.my_visit(formula)
    print("***After applying the visitor tree***")
    if(type(ded) == list):
        print('Approved')
        return ded
    print('Not approved')
    return False




class Deduct_Serializer(serializers.ModelSerializer):
    class Meta:
        model = deduction
        fields = ('id','created', 'formula', 'resolve_type')
    def create(self, data): pass
    def update(self, instance, data): pass


