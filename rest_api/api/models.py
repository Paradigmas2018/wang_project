#AUTHORS:

#Gabriel Lizano Alvarado
#Edwin Lobo Sánchez 
#Ruben Pereira Ugalde
#Luis Guillermo Ramírez Díaz

from django.db import models

# Create your models here.

TYPE_CHOICES = (
    ('1', 'WANG'),
    ('2', 'LORÍA'),
)

class deduction(models.Model):
    created = models.DateTimeField(auto_now = True, auto_now_add = False)
    formula = models.CharField(max_length = 200)
    resolve_type = models.CharField(max_length = 20, choices = TYPE_CHOICES)
    
    def __str__(self):
        return f"{self.formula} {self.resolve_type}"
    
    class Meta:
        ordering = ('formula',)

class DeductionDetail(models.Model):
    created = models.DateTimeField(auto_now_add = True)
    new_expr = models.CharField(max_length = 200)
    deduction = models.ForeignKey(deduction, on_delete = models.CASCADE)
    father_detail = models.ForeignKey(
                    "self",
                    models.SET_NULL,
                    blank = True,
                    null = True)
    rule_applied = models.CharField(max_length = 200, blank = True)

    def __str__(self):
        return f'{self.new_expr}, {self.deduction}, {self.father_detail}'

    class Meta:
        ordering = ('new_expr',)
