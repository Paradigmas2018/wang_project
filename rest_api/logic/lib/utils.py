from itertools import *

def replace(iter, pos, formulas):
    yield from islice(iter, 0, pos)
    yield from formulas
    yield from islice(iter, pos+1, None)

def first(iterable):
    """ Takes the first of iterable """
    for value in iterable:
        return value
    
    
#Aqui el yield from me abre la lista, si no lo utilizo, me pega toda la lista.

if __name__ == "__main__":
    m = [0]
    rep = [666]
    print(m, rep, list(replace(m, 0, rep)))