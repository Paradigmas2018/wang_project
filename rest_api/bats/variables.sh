#@echo off
#REM for export the pythonpath with our models.
export SOURCE=./logic/src
export LIB=./logic/lib
export PARSER=./logic/parser/grammar
export GRAMMAR=./logic/grammar
export PYTHONPATH=$SOURCE:$LIB:$PARSER:$GRAMMAR:$PYTHONPATH
echo *** PYTHONPATH=$PYTHONPATH ***

