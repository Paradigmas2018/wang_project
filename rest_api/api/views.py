#AUTHORS:

#Gabriel Lizano Alvarado
#Edwin Lobo Sánchez 
#Ruben Pereira Ugalde
#Luis Guillermo Ramírez Díaz

from django.shortcuts import render
from rest_framework import generics
from .models import deduction, DeductionDetail
from .deduction_serializer import DeductionSerializer
from .deduction_serializer import Deduct_Serializer
from .detail_serializer import DeductionDetailSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response


# Create your views here.

class DeductionAPIView(generics.ListCreateAPIView):
    queryset = deduction.objects.all()
    serializer_class = DeductionSerializer

class Deduction_APIView(generics.ListCreateAPIView):
    queryset = deduction.objects.all().order_by('id')
    serializer_class = Deduct_Serializer

class DeductionUpdDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = deduction.objects.all()
    serializer_class = DeductionSerializer

class DeductionSpecView(APIView):
    def get_object(self, pk):
        try:
            return deduction.objects.get(pk=pk)
        except deduction.DoesNotExist:
            raise Http404
    
    def get(self, request, pk, format = None):
        deduction = self.get_object(pk)
        serializer = DeductionSerializer(deduction)
        return Response(serializer.data)