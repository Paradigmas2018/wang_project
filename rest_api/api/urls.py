#AUTHORS:

#Gabriel Lizano Alvarado
#Edwin Lobo Sánchez 
#Ruben Pereira Ugalde
#Luis Guillermo Ramírez Díaz

from django.urls import path
from .views import *
from .view import *


# Create your urls here.

urlpatterns = [

    path('deduction/', DeductionAPIView.as_view()),
    path('deduction/all', Deduction_APIView.as_view()),
    path('deduction/<int:pk>', DeductionSpecView.as_view()),
    path('deduction/upd/<int:pk>', DeductionUpdDeleteView.as_view()),
    path('detail/', DeductionDetailAPIView.as_view()),
    path('detail/<int:pk>', DeductionDetailSpecView.as_view()),
    path('detail/upd/<int:pk>', DeductionDetailUpdDeleteView.as_view()),
    path('detail/spec/<int:deduction_id>', DeductionDetailView.as_view()),

]