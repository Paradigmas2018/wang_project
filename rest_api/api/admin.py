#AUTHORS:

#Gabriel Lizano Alvarado
#Edwin Lobo Sánchez 
#Ruben Pereira Ugalde
#Luis Guillermo Ramírez Díaz

from django.contrib import admin
from .models import *

# Register your models here.

class DeductionAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'formula', 'resolve_type')
    ordering = ('id',)
    search_fields = ('formula',)
    
    
class DeductionDetailAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'new_expr','deduction',
                    'father_detail', 'rule_applied')
    ordering = ('id',)
    search_fields = ('new_expr',)

admin.site.register(deduction, DeductionAdmin)
admin.site.register(DeductionDetail, DeductionDetailAdmin)
