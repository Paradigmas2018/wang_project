#AUTHORS:

#Gabriel Lizano Alvarado
#Edwin Lobo Sánchez 
#Ruben Pereira Ugalde
#Luis Guillermo Ramírez Díaz

from django.shortcuts import render
from rest_framework import generics
from .models import DeductionDetail
from .detail_serializer import DeductionDetailSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
import json

# Create your views here.

class DeductionDetailAPIView(generics.ListCreateAPIView):
    queryset = DeductionDetail.objects.all()
    serializer_class = DeductionDetailSerializer
    
class DeductionDetailUpdDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = DeductionDetail.objects.all()
    serializer_class = DeductionDetailSerializer
    
class DeductionDetailSpecView(APIView):
    def get_object(self, pk):
        try:
            return DeductionDetail.objects.get(pk = pk)
        except DeductionDetail.DoesNotExist:
            raise Http404
    
    def get(self, request, pk, format=None):
        deduction_detail = self.get_object(pk)
        serializer = DeductionDetailSerializer(deduction_detail)
        return Response(serializer.data)

class DeductionDetailView(APIView):
    def get_object(self, deduction_id):
        try:
            return DeductionDetail.objects.filter(deduction=deduction_id).order_by('id')
        except:
            raise Http404
    
    def get(self, request, deduction_id, format=None):
        # I need to use (many = True) because the query has many objects.
        deduction_detail = self.get_object(deduction_id)
        serializer = DeductionDetailSerializer(deduction_detail, many = True)
        return Response(serializer.data)

